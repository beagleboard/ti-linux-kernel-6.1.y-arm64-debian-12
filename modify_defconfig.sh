#!/bin/bash

cd ./linux/

echo "make ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- bb.org_defconfig"
make ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- bb.org_defconfig

echo "make ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- savedefconfig"
make ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- savedefconfig

cp -v defconfig compare_defconfig

echo "Config tweaks"
#./scripts/config --disable CONFIG_LOCALVERSION_AUTO
#./scripts/config --enable CONFIG_DEBUG_INFO_NONE
#./scripts/config --disable DEBUG_INFO_DWARF_TOOLCHAIN_DEFAULT

echo "make ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- olddefconfig"
make ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- olddefconfig

cp -v .config ../public/defconfig_pre_savedefconfig

echo "make ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- savedefconfig"
make ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- savedefconfig

cp -v defconfig ../public/defconfig

diff -u compare_defconfig defconfig

rm -rf compare_defconfig defconfig || true

cd ../
